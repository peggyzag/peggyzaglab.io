---
layout: page
title: Blog
permalink: /blog/
---

<div class="container">
	<div class="row">
		<div class="col-md-12">
			{% for post in site.posts %}
				{% include tile.html %}
			{% endfor %}
		</div>
	</div>
</div>
